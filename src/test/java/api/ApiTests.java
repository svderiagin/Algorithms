package api;

import org.testng.annotations.Test;

public class ApiTests {

    @Test(groups = "api")
    public void simpleApiTest() {
        System.out.println("*********************************************");
        System.out.println();
        System.out.println("It is the test method for ApiTests test class");
        System.out.println();
        System.out.println("*********************************************");
    }
}
