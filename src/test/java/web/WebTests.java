package web;

import org.testng.annotations.Test;

public class WebTests {

    @Test(groups = "web")
    public void simpleWebTest() {
        System.out.println("****************************************");
        System.out.println();
        System.out.println("It is the test method for WebTests class");
        System.out.println();
        System.out.println("****************************************");
    }
}
