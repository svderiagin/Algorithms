package unit;

import algorithms.TwoSum;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Array should be sorted
 */
public class TwoSumTests {
    @Test
    public void twoSumLoop() {
        int[] array = new int[]{-3, 0, 1, 3, 4};
        assertEquals(TwoSum.loop(array, 5), new int[]{1, 4});
    }

    @Test
    public void twoSumHashSet() {
        int[] array = new int[]{-3, 0, 1, 3, 4};
        assertEquals(TwoSum.hashSet(array, 5), new int[]{1, 4});
    }

    @Test
    public void twoSumBinarySearch() {
        int[] array = new int[]{-3, 0, 1, 3, 4};
        assertEquals(TwoSum.binarySearch(array, 5), new int[]{1, 4});
    }

    @Test
    public void twoSumTwoPointers() {
        int[] array = new int[]{-3, 0, 1, 3, 4};
        assertEquals(TwoSum.twoPointers(array, 5), new int[]{1, 4});
    }

    @Test
    public void twoSumTwoPointersClosestValues() {
        int[] array = new int[]{-3, 0, 1, 3, 30};
        assertEquals(TwoSum.twoPointersClosestValues(array, 5), new int[]{1, 3});
    }
}
