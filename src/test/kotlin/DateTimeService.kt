package unit

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

object DateTimeService {

    fun current(format: String = "d.M.yyyy", timeZone: String = "Europe/Berlin", locale: Locale = Locale.GERMAN): String {
        return LocalDateTime.now(ZoneId.of(timeZone)).format(DateTimeFormatter.ofPattern(format, locale))
    }

    fun plusDays(days: Long, format: String = "d.M.yyyy", timeZone: String = "Europe/Berlin", locale: Locale = Locale.GERMAN): String {
        return LocalDateTime.now(ZoneId.of(timeZone)).plusDays(days).format(DateTimeFormatter.ofPattern(format, locale))
    }
}