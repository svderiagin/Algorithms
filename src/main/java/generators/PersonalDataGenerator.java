package generators;

import io.qameta.allure.Step;

import java.util.Arrays;

public class PersonalDataGenerator {

    @Step("Сгенерировать ИНН")
    public static String generateInn() {
        var region = zeros(String.format("%.0f", Math.floor((Math.random() * 92) + 1)), 2);
        var inspection = zeros(String.format("%.0f", Math.floor((Math.random() * 99) + 1)), 2);
        var numba = zeros(String.format("%.0f", Math.floor((Math.random() * 99999) + 1)), 5);
        var result = region + inspection + numba;
        var resultAsArray = Arrays.stream(result.split("")).mapToInt(Integer::parseInt).toArray();

        String kontr = String.valueOf(((
                2 * resultAsArray[0] + 4 * resultAsArray[1] + 10 * resultAsArray[2] +
                        3 * resultAsArray[3] + 5 * resultAsArray[4] + 9 * resultAsArray[5] +
                        4 * resultAsArray[6] + 6 * resultAsArray[7] + 8 * resultAsArray[8]
        ) % 11) % 10);

        kontr = (kontr.equals("10")) ? "0" : kontr;

        return result + kontr;
    }

    @Step("Сгенерировать ОГРН")
    public static String generateOgrn() {
        var priznak = String.format("%.0f", Math.floor((Math.random() * 9) + 1));
        var godreg = zeros(String.format("%.0f", Math.floor((Math.random() * 16) + 1)), 2);
        var region = zeros(String.format("%.0f", Math.floor((Math.random() * 92) + 1)), 2);
        var inspection = zeros(String.format("%.0f", Math.floor((Math.random() * 99) + 1)), 2);
        var zapis = zeros(String.format("%.0f", Math.floor((Math.random() * 99999) + 1)), 5);
        var result = (priznak + godreg + region + inspection + zapis);
        var kontr = String.valueOf((Long.parseLong(result) % 11) % 10);
        kontr = (kontr.equals("10")) ? "0" : kontr;

        return result + kontr;
    }

    private static String zeros(String str, Integer lng) {
        var factLength = str.length();
        if (factLength < lng) {
            for (var i = 0; i < (lng - factLength); i++)
                str = "0" + str;
        }

        return str;
    }
}
