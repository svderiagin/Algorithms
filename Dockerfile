#FROM gradle:7.2.0-jdk11-hotspot
#
#WORKDIR /home/gradle/project
#
#EXPOSE 8080
#
#USER root
#
#
##Run apk update
#
#ENV GRADLE_USER_HOME /home/gradle/project
#
#COPY . /home/gradle/project
#
#RUN gradle build -x test
#
#FROM java:jre-alpine
#
#WORKDIR /home/gradle/project
#
#COPY --from=0 /home/gradle/project/build/libs/project-0.0.1-SNAPSHOT.jar .
#
#ENTRYPOINT java -jar project-0.0.1-SNAPSHOT.jar

FROM openjdk:11-jdk-slim

WORKDIR /app

COPY build/libs/coding_tools-1.0-SNAPSHOT.jar  app.jar

ENTRYPOINT java -jar app.jar